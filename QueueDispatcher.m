classdef QueueDispatcher < handle
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        bdAcc
        tipoBranew
        dadosBranew
        username
    end
    
    methods
        function self = QueueDispatcher()
        end
        function connectBD(self)
            self.bdAcc = database('Queue','SA','Engenho1');
        end
        function dispatcher(self)
            self.connectBD();
            while(true)
                dados = self.leBD();
                if ~strcmp(dados.Data(1,1),'No Data')
                    self.username = dados.Data{1,1};
                    self.tipoBranew = dados.Data{1,3};
                    load(['D:\Users\Administrator\Desktop\webportal\matfiles\' dados.Data{1,2}]);
                    self.dadosBranew = handles;
                    if strcmp(self.tipoBranew,'branew')
                        addpath('D:\Users\Administrator\Desktop\webportal\ML\Branew');
                        handles = le_dados(self.dadosBranew);
                        handles = otimizar(handles);
                        nome_mat = self.escreveBD(self.tipoBranew);
                        save(['D:\Users\Administrator\Desktop\webportal\matfiles\' nome_mat],'handles');
                        rmpath('D:\Users\Administrator\Desktop\webportal\ML\Branew');
                    else
                        addpath('D:\Users\Administrator\Desktop\webportal\ML\BranewEPE');
                        handles = le_dados(self.dadosBranew);
                        handles = otimizar(handles);
                        nome_mat = self.escreveBD(self.tipoBranew);
                        save(['D:\Users\Administrator\Desktop\webportal\matfiles\' nome_mat],'handles');
                        rmpath('D:\Users\Administrator\Desktop\webportal\ML\BranewEPE');
                    end
                end
                
                pause(5);
            end
            
            
        end
        
        function dado = leBD(self)
            SQLcmd = ['select username,entrada_dados, branew_tipo from queue_branew_generico where status = 0 ORDER BY queue_datetime ASC'];
            curs = exec(self.bdAcc,SQLcmd);
            dado=fetch(curs);
        end
        
        function nome_mat = escreveBD(self, branew_tipo)
            nome_mat = ['resultados' branew_tipo self.username '.mat'];
            SQLcmd = ['UPDATE queue_branew_generico SET saida_dados=''' nome_mat ...
                ''' WHERE username =''' ...
                self.username ''' AND status = 0 AND  branew_tipo=''' branew_tipo ''''];
            curs = exec(self.bdAcc,SQLcmd);
            SQLcmd = ['UPDATE queue_branew_generico SET status=1 WHERE username =''' ...
                self.username ''' AND status = 0'];
            curs = exec(self.bdAcc,SQLcmd);
            
        end
    end
    
    
end

